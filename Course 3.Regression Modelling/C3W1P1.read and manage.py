import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

data_dir = os.environ['COURSERA_DATA_DIR']
df = pd.read_csv(os.path.join(data_dir, 'addhealth_pds.csv'), low_memory = False)
df.columns = map(str.lower, df.columns)
print(len(df))

# h1rp1 = perception of risk of pregnancy (section 8)
# h1pa3 = perception of parental disapproval of contraceptives (section 34)
# h1co3 = use of contraception (section 24)
short_labels = {'h1rp1': 'risk perception',
                'h1co3': 'contraceptive use',
                'h1pa3': 'mother disapproves'}

# value of 7 codes legitimate skip for all three questions, exclude from dataset
df = df[(df['h1rp1'] != 7) & (df['h1pa3'] != 7) & (df['h1co3'] != 7)]

# values of 6 (refused), 8 (don't know) and 9 (not applicable) are coded as missing values
nan_values = (6, 8, 9)
for col in short_labels:
	for nan_value in nan_values:
		df[col] = df[col].replace(nan_value, np.nan)
	df[col] = df[col].astype('category')

sub1 = df[list(short_labels.keys())]
print(sub1[:3])
print(len(sub1))

# label values in each field
five_categories = ['strongly agree', 'agree', 'neither', 'disagree', 'strongly disagree']
two_categories = ['no', 'yes']
parental_five = ['strongly disapprove', 'disapprove', 'neither', 'approve', 'strongly approve']
sub1['h1rp1'] = sub1['h1rp1'].cat.rename_categories(five_categories)
sub1['h1pa3'] = sub1['h1pa3'].cat.rename_categories(parental_five)
sub1['h1co3'] = sub1['h1co3'].cat.rename_categories(two_categories)

# count the valid values
for field in short_labels.keys():
	print(field, short_labels[field])
	ct = sub1.groupby(field).size()
	print(ct)
	print(ct*100/len(sub1))
	print()

# count the nan values
print(sub1.isnull().sum())

sns.countplot(x = 'h1rp1', data = sub1)
plt.xlabel(short_labels['h1rp1'])
sns.plt.show()  

sns.countplot(x = 'h1co3', data = sub1)
plt.xlabel(short_labels['h1co3'])
sns.plt.show() 

sns.countplot(x = 'h1pa3', data = sub1)
plt.xlabel(short_labels['h1pa3'])
sns.plt.show() 