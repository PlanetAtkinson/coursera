#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 11:56:08 2017

@author: ian

Modelling quantitative on both exp and resp vars
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import seaborn as sns

data_dir = os.environ['COURSERA_DATA_DIR']
df = pd.read_csv(os.path.join(data_dir, 'gapminder.csv'))

sub2 = pd.DataFrame()
sub2.dropna()

# make all vars two-valued categoricals
for var in ['breastcancerper100th', 'alcconsumption', 'incomeperperson', 'lifeexpectancy',
            'polityscore', 'urbanrate']:
    sub2[var] = pd.to_numeric(df[var], errors = 'coerce')
    median = sub2[var].quantile(0.5)
    sub2[var] = sub2[var].map(lambda x: 0 if x < median else 1)

# first, the basic 1-factor model
lreg1 = smf.logit(formula = 'breastcancerper100th ~ alcconsumption', data = sub2).fit()
print(lreg1.summary())
conf = lreg1.conf_int()
conf['OR'] = lreg1.params
conf.columns = ['Lower CI', 'Upper CI', 'OR']
print(np.exp(conf))
print()

# next, a two-factor model
lreg2 = smf.logit(formula = 'breastcancerper100th ~ alcconsumption + incomeperperson', 
                  data = sub2).fit()
print(lreg2.summary())
conf = lreg2.conf_int()
conf['OR'] = lreg2.params
conf.columns = ['Lower CI', 'Upper CI', 'OR']
print(np.exp(conf))
print()

# three-factor model
lreg3 = smf.logit(formula = 
                  'breastcancerper100th ~ alcconsumption + incomeperperson + lifeexpectancy', 
                  data = sub2).fit()
print(lreg3.summary())
conf = lreg3.conf_int()
conf['OR'] = lreg3.params
conf.columns = ['Lower CI', 'Upper CI', 'OR']
print(np.exp(conf))
print()

# still everything is significant!
lreg4 = smf.logit(formula = 'breastcancerper100th ~ alcconsumption + incomeperperson + \
                  lifeexpectancy + polityscore', 
                  data = sub2).fit()
print(lreg4.summary())
conf = lreg4.conf_int()
conf['OR'] = lreg4.params
conf.columns = ['Lower CI', 'Upper CI', 'OR']
print(np.exp(conf))
print()

# five factor model
lreg5 = smf.logit(formula = 'breastcancerper100th ~ alcconsumption + incomeperperson + \
                  lifeexpectancy + polityscore + urbanrate', 
                  data = sub2).fit()
print(lreg5.summary())
conf = lreg5.conf_int()
conf['OR'] = lreg5.params
conf.columns = ['Lower CI', 'Upper CI', 'OR']
print(np.exp(conf))
print()

# we see that urbanrate is not significantly associated with breast cancer,
# after controlling for the other variables.
# but we also see that incomeperperson is confounded with urbanrate.
# so it could be that replacing incomeperperson with urbanrate in the 
# four factor model will give a better four factor model
lreg4b = smf.logit(formula = 'breastcancerper100th ~ alcconsumption + urbanrate + \
                  lifeexpectancy + polityscore', 
                  data = sub2).fit()
print(lreg4b.summary())
conf = lreg4b.conf_int()
conf['OR'] = lreg4b.params
conf.columns = ['Lower CI', 'Upper CI', 'OR']
print(np.exp(conf))
print()

# yep, lreg4b is a little bit better than lreg4
# it's also better than lreg5 in terms of p-value, but not r-squared.
# neither of the four-factor models had any non-overlapping Odds Ratio intervals,
# so we can't say that any of the exp vars is more significant than the others.

# This entire process could be automated into a single procedure.

print('***************************************************************************')
