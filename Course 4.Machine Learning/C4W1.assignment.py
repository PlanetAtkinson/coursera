#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 19:47:03 2017

@author: ian
"""

from pandas import Series, DataFrame
import pandas as pd
import numpy as np
import os
import matplotlib.pylab as plt
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
import sklearn.metrics

data_dir = os.environ['COURSERA_DATA_DIR']

df = pd.read_csv(os.path.join(data_dir, "gapminder.csv"))
sub = pd.DataFrame()

# make response var a two-valued categorical
sub['breastcancer'] = pd.to_numeric(df['breastcancerper100th'], errors = 'coerce')
median = sub['breastcancer'].quantile(0.5)
sub['breastcancer'] = sub['breastcancer'].map(lambda x: 0 if x < median else 1)

# put our explanatory vars into subset
for var in ['alcconsumption', 'incomeperperson', 'lifeexpectancy',
            'polityscore', 'urbanrate']:
    sub[var] = pd.to_numeric(df[var], errors = 'coerce')
sub = sub.dropna()

# setting aside 40% of data for testing
predictors = sub[['alcconsumption', 'incomeperperson', 'lifeexpectancy',
            'polityscore', 'urbanrate']]
targets = sub['breastcancer']
pred_train, pred_test, tar_train, tar_test  =   train_test_split(
        predictors, targets, test_size=.4, random_state = 123)
#logit_train = pd.concat([pred_train, tar_train], axis = 1)
#logit_test = pd.concat([pred_test, tar_test], axis = 1)

print(pred_train.shape)
print(pred_test.shape)
print(tar_train.shape)
print(tar_test.shape)

#Build model on training data
classifier=DecisionTreeClassifier(max_leaf_nodes = 20)
classifier=classifier.fit(pred_train,tar_train)

# get predictions from model
predictions=classifier.predict(pred_test)
print(predictions) # is a numpy array of 1s and 0s

print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))
# with 5 leafs, accuracy is 74.4%
# with 10 leafs, accuracy is 78.0%
# with 20 leafs, accuracy is 76.7%
# with 100 leafs, accuracy is 76.7%
# with 1000 leafs, accuracy is 76.7%

for leafs in range(5,20):
    classifier = DecisionTreeClassifier(max_leaf_nodes = leafs)
    classifier = classifier.fit(pred_train, tar_train)
    predictions = classifier.predict(pred_test)
    print(leafs, sklearn.metrics.accuracy_score(tar_test, predictions))
# for this seed (does change with different seeds),
# 10 is the lowest number with the highest accuracy
# odd that adding another leaf gives identical accuracy 
# - is my data insufficiently granular?

classifier10 = DecisionTreeClassifier(max_leaf_nodes = 10).fit(pred_train, tar_train)
classifier11 = DecisionTreeClassifier(max_leaf_nodes = 11).fit(pred_train, tar_train)

    


# generating an image of the tree is a mouthful of syntax
#Displaying the decision tree
from sklearn import tree
from io import StringIO
import pydotplus
from IPython.display import Image
out = StringIO()

tree.export_graphviz(classifier10, out_file = 'tree.dot')

tree.export_graphviz(classifier10, out_file=out)
graph=pydotplus.graph_from_dot_data(out.getvalue())
Image(graph.create_png())
print('done')


