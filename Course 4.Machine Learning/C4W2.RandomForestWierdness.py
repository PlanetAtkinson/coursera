import os
import pandas as pd
import numpy as np
import os
import matplotlib.pylab as plt
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier
import sklearn.metrics
from sklearn.ensemble import ExtraTreesClassifier
import random


data_dir = os.environ['COURSERA_DATA_DIR']
df = pd.read_csv(os.path.join(data_dir, 'nesarc_sub.csv'), 
                 low_memory = False, na_values = [' ', ''])
df = df.dropna()

df = df.drop('Unnamed: 0', axis = 1) # don't know where this rubbish came from

'''
Aim is to test if it's treating BUILDTYP as continuous or categorical.
And does changing it to string affect this.
'''

targets = df.MAJORDEPLIFE

predictors = df['BUILDTYP'].reshape(-1,1) # reshape due to error msg when single feature

print('\nstandard usage, categorical behaviour')
pred_train, pred_test, tar_train, tar_test  = train_test_split(predictors, targets, 
	test_size=.4, random_state = 123)
classifier = RandomForestClassifier(n_estimators = 25).fit(
        pred_train, tar_train)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))

print('\ndummy vars make no difference, it\'s categorical')
dummies = pd.get_dummies(df['BUILDTYP'])
print(dummies[:3])
dummies.columns = ['BUILDTYP_' + col for col in dummies.columns.astype(str)]
pred_train, pred_test, tar_train, tar_test = train_test_split(
        dummies, targets, test_size = 0.4, random_state = 123)
classifier = RandomForestClassifier(n_estimators = 25).fit(
        pred_train, tar_train)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))

print('\nconverting to float makes no difference')
float_predictors = df['BUILDTYP'].apply(lambda x: float(x)).reshape(-1,1)
pred_train, pred_test, tar_train, tar_test = train_test_split(
        float_predictors, targets, test_size = 0.4, random_state = 123)
classifier = RandomForestClassifier(n_estimators = 25).fit(
        pred_train, tar_train)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))

print('\nstring makes no difference')
string_predictors = predictors.astype(str)
pred_train, pred_test, tar_train, tar_test  = train_test_split(
        string_predictors, targets, test_size=.4, random_state = 123)
classifier = RandomForestClassifier(n_estimators = 25).fit(
        pred_train, tar_train)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))

'''
# unconvertable string generates error
print(string_predictors[:3])
outer = []
for c in string_predictors:
    outer.append(['p' + c[0]])
very_string_predictors = np.array(outer)
print(very_string_predictors[:3])

pred_train, pred_test, tar_train, tar_test  = train_test_split(
        very_string_predictors, targets, test_size=.4, random_state = 123)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))
'''

print('\nif everything is categorical, then changing the numbers around won\'t alter the outcome')
swapped_predictors = df['BUILDTYP'].apply(
        lambda x: int(x/2) if x % 2 == 0 else int(3 * x)).reshape(-1,1) 
pred_train, pred_test, tar_train, tar_test = train_test_split(
        swapped_predictors, targets, test_size = 0.4, random_state = 123)
print(pred_train.shape)
print(pred_test.shape)
print(tar_train.shape)
print(tar_test.shape)
classifier = RandomForestClassifier(n_estimators = 25).fit(
        pred_train, tar_train)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))

print('\nif it\'s categorical, small perturbations for float values should make massive difference')
perturbed_predictors = df['BUILDTYP'].apply(
        lambda x: float(x) + random.uniform(0.000001, 0.0000001)).reshape(-1,1)
pred_train, pred_test, tar_train, tar_test = train_test_split(
        perturbed_predictors, targets, test_size = 0.4, random_state = 123)
print(pred_train.shape)
print(pred_test.shape)
print(tar_train.shape)
print(tar_test.shape)
classifier = RandomForestClassifier(n_estimators = 25).fit(
        pred_train, tar_train)
predictions = classifier.predict(pred_test)
print(sklearn.metrics.confusion_matrix(tar_test,predictions))
print(sklearn.metrics.accuracy_score(tar_test, predictions))
