import argparse
import pandas as pd

parser = argparse.ArgumentParser(description = 'read a csv file')
parser.add_argument('csv_filename', help = 'name of csv file to summarise')
args = parser.parse_args()

df = pd.read_csv(args.csv_filename, low_memory = False)
print(df)
print(df.describe())


