import os
import argparse
import statistics

parser = argparse.ArgumentParser(description = 'read a csv file')
parser.add_argument('csv_filename', help = 'name of csv file to summarise')
args = parser.parse_args()

lines = [line.split(',') for line in open(args.csv_filename).read().splitlines()]
headers = lines[0]
print(headers)
data = lines[1:]

print(dir(statistics))

ages = [int(d[1]) for d in data]
print(statistics.mean(ages))


